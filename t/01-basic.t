use strict;
use warnings;
use Test::More 0.96;

use Types::StandardX::URI qw(
    URI
    URIhttp
    URIhttps
);

{
    note 'Test file URI';
    my $str = 'file://foo.xml';

    my $uri = URI->coerce($str);
    ok($uri, "URI->coerce($str) is a valid URI object");
    ok(URI->check($uri), "$str is file uri");
    ok(!URIhttp->check($uri), "$str is not http uri");
    ok(!URIhttps->check($uri), "$str is not https uri");
}

{
    note 'Test HTTP URI';
    my $str = 'http://foo.example.com/foo';

    my $uri = URIhttp->coerce($str);
    ok($uri, "URIhttp->coerce($str) is a valid URI object");
    ok(URI->check($uri), "$str is file uri");
    ok(URIhttp->check($uri), "$str is  http uri");
    ok(!URIhttps->check($uri), "$str is not https uri");
}

{
    note 'Test HTTPS URI';
    my $str = 'https://foo.example.com';

    my $uri = URIhttps->coerce($str);
    ok($uri, "URIhttps->coerce($str) is a valid URI object");
    ok(URI->check($uri), "$str is file uri");
    ok(URIhttp->check($uri), "$str is http uri");
    ok(URIhttps->check($uri), "$str is https uri");
}



done_testing;
