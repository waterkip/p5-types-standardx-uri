use utf8;
package Types::StandardX::URI;
our $VERSION = '0.001';
use warnings;
use strict;

# ABSTRACT: Moo(se) like URI type

use Types::Standard qw(Str);
use URI;
use Type::Utils -all;
use Type::Library
    -base,
    -declare => qw(
        URI
        URIhttp
        URIhttps
    );
use List::Util qw(any);

class_type URI, {class => 'URI' };
coerce URI, from Str, via { return URI->new($_); };

subtype URIhttp, as URI,
    where {
        my $scheme = $_->scheme;
        return any { $_ eq $scheme } qw(http https)
    };
coerce URIhttp, from Str, via { return URI->new($_); };

subtype URIhttps, as URI,
    where {
        return $_->scheme eq 'https';
    };
coerce URIhttps, from Str, via { return URI->new($_); };

1;

__END__

=head1 DESCRIPTION

Default URI Types for Moo(se).

=head1 SYNOPSIS

    use Types::StandardX::URI qw(
        URI
        URIHttp
        URIHttps
    );

    URI->check(..);
    URI->assert(..);

=head1 TYPES

=head2 URI

A default L<URI> object

=head2 URIHttp

An L<URI> object which is either the http or https scheme

=head2 URIHttps

An L<URI> object which is the https scheme
